<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property int|null $idcochealquilado
 * @property string|null $fechaalquiler
 *
 * @property Coches $idcochealquilado0
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['cod', 'idcochealquilado'], 'integer'],
            [['fechaalquiler'], 'safe'],
            [['nombre'], 'string', 'max' => 45],
            [['idcochealquilado'], 'unique'],
            [['cod'], 'unique'],
            [['idcochealquilado'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['idcochealquilado' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'idcochealquilado' => 'Idcochealquilado',
            'fechaalquiler' => 'Fechaalquiler',
        ];
    }

    /**
     * Gets query for [[Idcochealquilado0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdcochealquilado0()
    {
        return $this->hasOne(Coches::className(), ['id' => 'idcochealquilado']);
    }
}
