﻿DROP DATABASE IF EXISTS aplicacion3;
CREATE DATABASE aplicacion3;
USE aplicacion3;

CREATE TABLE coches(
id int,
marca varchar(45),
fecha date,
precio float,
PRIMARY KEY(id) 
);

CREATE TABLE clientes(
cod int,
nombre varchar(45),
idcochealquilado int,
fechaalquiler date,
PRIMARY KEY(cod),
UNIQUE KEY(idcochealquilado)
);

ALTER TABLE clientes
ADD CONSTRAINT fkcochesclientes
FOREIGN KEY(idcochealquilado) REFERENCES coches(id)
ON DELETE CASCADE ON UPDATE CASCADE;
